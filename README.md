# Reservas Online

### Hi! this project is currently in development and this README is temporary, but here are some screenshots:

### - Login page
<div align="center">
    <img alt="Login" src="https://i.imgur.com/hwOLlaz.png" />
</div>

### - Register page
<div align="center">
    <img alt="Registro" src="https://i.imgur.com/uld9BSZ.png" />
</div>

### - Main page with some ficticius restaurants on the list
<div align="center">
    <img alt="Main" src="https://i.imgur.com/oSBEuLn.png" />
</div>

### - Restaurant profile
<div align="center">
    <img alt="Perfil do restaurante" src="https://i.imgur.com/QXzahjX.png" />
</div>

### - User settings
<div align="center">
    <img alt="Perfil do usuário" src="https://i.imgur.com/CUTqhGf.png" />
</div>