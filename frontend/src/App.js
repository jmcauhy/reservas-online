import React from 'react'
import { createGlobalStyle } from 'styled-components'

import Routes from './routes'

const App = () => <> <GlobalStyle /> <Routes /> </>

const GlobalStyle = createGlobalStyle`
   * {
      margin: 0;
      padding: 0;
      outline: none;
      box-sizing: border-box;
   }

   body {
      font-size: 14px;
      font-family: 'Open Sans', sans-serif;
      background-color: #EEEEF8;
      -webkit-font-smoothing: antialiased;
   }

   input, select, button {
      width: 100%;
      font-size: 16px;
      font-family: 'Open Sans', sans-serif;
      border-radius: 8px;
   }

   button {
      /* --- FLEX --- */
      display: flex;
      align-items: center;
      justify-content: center;
      /*--------------*/
      height: 50px;
      text-decoration: none;
      color: #FFF;
      letter-spacing: .5px;
      border: 0;
      cursor: pointer;
      transition: .2s;
   }

   input {
      height: 50px;
      padding: 0 15px;
      color: #333;
      border: 2px solid #FFF;
   }

   input:focus { border: 2px solid #0059b3; }

   select {
      height: 50px;
      padding: 0 10px;
      color: #333;
      background-color: #FFF;
      border: 2px solid #FFF;
   }

   select:focus { border: 2px solid #0059b3; }

   a {
      text-decoration: none;
      color: #FFF;
   }
`

export default App