import React, { useContext } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import { AuthProvider, AuthContext } from './Contexts/AuthContext'
import { Sign, Main, Restaurant, UserSettings } from './pages'

const CustomRoute = ({ isPrivate, ...rest }) => {
   const { isLoading, isAuth } = useContext(AuthContext)

   if (isLoading) return 'Carregando...'

   if (isPrivate && !isAuth) return <Redirect to="/" />

   return <Route {...rest} />
}

const Routes = () => (
   <Router>
      <AuthProvider>
         <Switch>
            <CustomRoute exact path="/" component={Sign} />
            <CustomRoute isPrivate path="/restaurantes" component={Main} />
            <CustomRoute isPrivate path="/restaurante" component={Restaurant} />
            <CustomRoute isPrivate path="/config" component={UserSettings} />
         </Switch>
      </AuthProvider>
   </Router>
)

export default Routes