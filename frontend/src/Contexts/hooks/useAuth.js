import { useState, useEffect } from 'react'

import { useHistory } from 'react-router-dom'
import api from '../../services/api'

const useAuth = () => {
   const [isAuth, setIsAuth] = useState(false),
         [isLoading, setIsLoading] = useState(true)

   const history = useHistory()

   useEffect(() => {
      const token = localStorage.getItem('token')

      if (token) {
         api.defaults.headers.Authorization = `Bearer ${JSON.parse(token)}`
         setIsAuth(true)
      }

      setIsLoading(false)
   }, [])

   const handleLogin = async (email, password) => {
      try {
         const { data } = await api.post('/auth', { email, password })

         localStorage.setItem('token', JSON.stringify(data.token))
         api.defaults.headers.Authorization = `Bearer ${data.token}`
         setIsAuth(true)

         history.push('/restaurantes')
      }
      catch(err) { console.log(err.message) }
   }

   const handleLogout = () => {
      try {
         localStorage.removeItem('token')
         api.defaults.headers.Authorization = undefined
         setIsAuth(false)

         history.push('/')
      }
      catch(err) { console.log(err.message) }
   }

   return { isLoading, isAuth, handleLogin, handleLogout }
}

export default useAuth