import React, { createContext } from 'react'
import useAuth from './hooks/useAuth'

const AuthContext = createContext()

const AuthProvider = ({ children }) => {
   const { isLoading, isAuth, handleLogin, handleLogout } = useAuth()

   return (
      <AuthContext.Provider value={{ isLoading, isAuth, handleLogin, handleLogout }}>
         {children}
      </AuthContext.Provider>
   )
}

export { AuthContext, AuthProvider }