import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

import { AuthContext } from '../../Contexts/AuthContext'

import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'

const LoginForm = ({ setSign }) => {
   const { handleLogin } = useContext(AuthContext)

   const [isVisible, setIsVisible] = useState(false),
         handlePasswordVisibility = () => setIsVisible(!isVisible)

   return (
      <Formik
         initialValues={{ email: '', password: '' }}

         validationSchema={
            Yup.object().shape({
               email: Yup.string().email().max(48).required(),
               password: Yup.string().min(8).max(32).required()
            })
         }

         onSubmit={({ email, password }) => handleLogin(email, password)}
      >
         {({ touched, errors }) => (
            <Form>
               <EmailContainer>
                  <Field 
                     style={touched.email && errors.email && { borderColor: '#d00' }}
                     type="email"
                     name="email"
                     maxLength="48"
                     placeholder="E-mail"
                  />
               </EmailContainer>

               <PasswordContainer>
                  <PasswordInput
                     style={touched.password && errors.password && { borderColor: '#d00' }}
                     type={isVisible ? 'text' : 'password'}
                     name="password"
                     className="password"
                     maxLength="32"
                     placeholder="Senha"
                  />
                  <div onClick={handlePasswordVisibility}>
                     {isVisible ? <VisibilityOff /> : <VisibilityOn />}
                  </div>
               </PasswordContainer>

               <LoginButton type="submit"> Entrar </LoginButton>

               <RegisterLink onClick={() => setSign('register')}>
                  Ainda não sou cadastrado
                  <ArrowForwardIcon style={{ marginLeft: 8, fontSize: 20 }} />
               </RegisterLink>
            </Form>
         )}
      </Formik>
   )
}

const EmailContainer = styled.div` margin-top: 25px; `

const PasswordContainer = styled.div`
   position: relative;
   height: 50px;
   margin-top: 15px;
`

const PasswordInput = styled(Field)`
   position: absolute;
   padding-right: 50px;
`

const VisibilityOn = styled(VisibilityIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const VisibilityOff = styled(VisibilityOffIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const LoginButton = styled.button`
   margin-top: 15px;
   background-color: #0059b3;
   &:hover { background-color: #004080 }
`

const RegisterLink = styled.a`
   /* - FLEX - */
   display: flex;
   align-items: center;
   justify-content: center;
   /*----------*/
   margin-top: 20px;
   color: #444;
   font-weight: bolder;
   font-size: 14px;
   font-family: Roboto, sans-serif;
   transition: .2s;
   cursor: pointer;
   &:hover { color: #0a0 }
`

export default LoginForm