import React, { useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components'

import { AuthContext } from '../../Contexts/AuthContext'
import LoginForm from './LoginForm'
import RegisterForm from './RegisterForm'

const Sign = () => {
   const { isAuth } = useContext(AuthContext)

   const [sign, setSign] = useState('login')

   if (isAuth) return <Redirect to='/restaurantes' />

   return (
      <Container>
         <Content>
            <Header>
               <Logo>Reservas Online</Logo>
               <Subtitle>Prioridade nos melhores restaurantes.</Subtitle>
            </Header>

            {  sign === 'login' ?
               <LoginForm setSign={setSign} />
               :
               <RegisterForm setSign={setSign} />
            }
         </Content>
      </Container>
   )
}

const Container = styled.div`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: center;
   /*--------------*/
   position: absolute;
   width: 100%;
   height: 100vh;
   background-color: #00264d;
   background-image: url('https://i.imgur.com/9SGMeYj.jpg');
   background-size: cover;
   background-repeat: no-repeat;

   @media (min-width: 960px) {
      width: 960px;
      height: 650px;
      top: 50%;
      margin-top: -325px;
      left: 50%;
      margin-left: -480px;
      background-position: 0 -350px;
      border-radius: 12px;
      box-shadow: 0px 0px 25px 0px rgba(0,0,0,.5);
   }
`

const Content = styled.div`
   width: 85%;
   min-width: 300px;
   max-width: 400px;
   margin-top: -25px;
   padding: 20px;
   background-color: rgba(245,245,255,.8);
   border-radius: 10px;

   @media (min-width: 450px) { margin-top: 0; }
`

const Header = styled.header` width: 100%; `

const Logo = styled.h1`
   text-align: center;
   color: #004080;
   font-size: 36px;
   font-family: Girassol, sans-serif;
   cursor: default;
`

const Subtitle = styled.h2`
   margin-bottom: 10px;
   padding-left: 5px;
   text-align: center;
   color: #111;
   font-weight: lighter;
   font-family: serif;
   font-size: 15px;
   font-family: Roboto, sans-serif;
   cursor: default;
`

export default Sign