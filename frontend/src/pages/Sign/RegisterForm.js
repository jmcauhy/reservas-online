import React, { useState } from 'react'
import styled from 'styled-components'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

import api from '../../services/api'
import { UFs } from '../../utils/UFs.json'

import PersonAddIcon from '@material-ui/icons/PersonAdd'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'

const RegisterForm = ({ setSign }) => {
   const [isVisible, setIsVisible] = useState(false),
         handlePasswordVisibility = () => setIsVisible(!isVisible)

   return (
      <Formik
         initialValues={{ name: '', uf: '', email: '', password: '' }}

         validationSchema={
            Yup.object().shape({
               name: Yup.string().min(3).max(32).required(),
               uf: Yup.string().required(),
               email: Yup.string().email().max(48).required(),
               password: Yup.string().min(8).max(32).required()
            })
         }

         onSubmit={ async ({ name, email, uf, password }) => {
            try {
               await api.post('/user', { name, uf, email, password })

               alert('Cadastro efetuado com sucesso, faça seu login!')
               setSign('login')
            }
            catch(err) { console.log(err.message) }
         }}
      >
         {({ touched, errors }) => (
            <Form>
               <NameContainer>
                  <Field
                     style={touched.name && errors.name && { borderColor: '#d00' }}
                     type="name"
                     name="name"
                     className="name"
                     maxLength="32"
                     placeholder="Nome"
                  />
               </NameContainer>

               <UfContainer>
                  <Field
                     component="select"
                     name="uf"
                     style={touched.uf && errors.uf && { borderColor: '#d00' }}
                  >
                     <option> Estado </option>
                     {UFs.map(uf => <option key={uf.id} value={uf.initials}> {uf.name} </option>)}
                  </Field>
               </UfContainer>

               <EmailContainer>
                  <Field
                     style={touched.email && errors.email && { borderColor: '#d00' }}
                     type="email"
                     name="email"
                     maxLength="48"
                     placeholder="E-mail"
                  />
               </EmailContainer>

               <PasswordContainer>
                  <PasswordInput
                     style={touched.password && errors.password && { borderColor: '#d00' }}
                     type={isVisible ? 'text' : 'password'}
                     name="password"
                     className="password"
                     maxLength="32"
                     placeholder="Senha"
                  />
                  <div onClick={handlePasswordVisibility}>
                     {isVisible ? <VisibilityOff /> : <VisibilityOn />}
                  </div>
               </PasswordContainer>

               <RegisterButton type="submit">
                  Cadastrar
                  <PersonAddIcon style={{ marginLeft: 8 }} />
               </RegisterButton>

               <GoToLogin onClick={() => setSign('login')}>
                  <ArrowBackIcon style={{ marginRight: 8, fontSize: 20 }} />
                  Voltar para a página inicial
               </GoToLogin>
            </Form>
         )}
      </Formik>
   )
}

const NameContainer = styled.div` margin-top:25px; `

const UfContainer = styled.div` margin-top: 15px; `

const EmailContainer = styled(UfContainer)``

const PasswordContainer = styled.div`
   position: relative;
   height: 50px;
   margin-top: 15px;
`

const PasswordInput = styled(Field)`
   position: absolute;
   padding-right: 50px;
`

const VisibilityOn = styled(VisibilityIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const VisibilityOff = styled(VisibilityOffIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const RegisterButton = styled.button`
   margin-top: 15px;
   color: #FFF;
   background-color: #0a0;

   &:hover {
      color: #FFF;
      background-color: #080;
   }
`

const GoToLogin = styled.a`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: center;
   /*--------------*/
   margin-top: 20px;
   color: #444;
   font-weight: bolder;
   font-size: 14px;
   font-family: Roboto, sans-serif;
   transition: .2s;
   cursor: pointer;

   &:hover { opacity: .7 }
`

export default RegisterForm