import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import StarRateIcon from '@material-ui/icons/StarRate'

const RestaurantCard = ({ id, logo, name, stars, category, description }) => {
   return (
      <Link to={`/restaurante?id=${id}`}>
         <Item>
            <Logo src={logo} alt={name} />
            <Info>
               <NameAndStars>
                  <Name> {name} </Name>
                  <Stars><StarRateIcon /> {stars} </Stars>
               </NameAndStars>

               <Category> {category} </Category>

               <Description> {description} </Description>
            </Info>
         </Item>
      </Link>
   )
}

const Item = styled.li`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   /*--------------*/
   width: 100%;
   height: 110px;
   padding: 10px;
   background-color: #FFF;
   border: 2px solid #DDE;
   border-radius: 10px;
   
   @media (min-width: 750px) { grid-column: span 1 }
`

const Logo = styled.img`
   width: 70px;
   height: 70px;
   border-radius: 50%;
   border: 1px solid #DDE;
`

const Info = styled.div`
   width: 100%;
   margin-left: 10px;
   padding: 0 10px;
   border-left: 1px solid #DDE;
`

const NameAndStars = styled.div`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: space-between;
   /*--------------*/
`

const Name = styled.h1`
   color: #445;
   font-size: 15px;
   font-family: Roboto, sans-serif;
   letter-spacing: .5px;
`

const Stars = styled.p`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: space-between;
   /*--------------*/
   margin-bottom: 3px;
   color: #FA0;
   font-size: 15px;
`

const Category = styled.p`
   color: #666;
   font-size: 13px;
`

const Description = styled.p`
   height: 40px;
   padding-top: 5px;
   color: #666;
   font-size: 12px;
   overflow: hidden;
`

export default RestaurantCard