import React, { useRef, useEffect } from 'react'
import { Formik, Form, Field } from 'formik'
import styled from 'styled-components'

import { UFs } from '../../utils/UFs.json'

const ChangeLocation = ({ isPopUp, setIsPopUp, setUf }) => {
   const ref = useRef()

   useEffect(() => {
      const handlePopUpDisplay = e => {
         if (ref.current.contains(e.target)) return

         setIsPopUp(!isPopUp)
      }

      document.addEventListener('mouseup', handlePopUpDisplay)

      return () => document.removeEventListener('mouseup', handlePopUpDisplay)
   }, [isPopUp, setIsPopUp])

   return (
      <PopUp>
         <Formik
            initialValues={{ uf: '' }}

            onSubmit={({ uf }) => {
               localStorage.setItem('UF', uf)
               setUf(uf)
               setIsPopUp(!isPopUp)
            }}
         >
            {() => (
               <StyledForm ref={ref}>
                  <SelectUf component="select" name="uf">
                     <option> Estado </option>
                     {UFs.map(uf => <option key={uf.id} value={uf.initials}> {uf.name} </option>)}
                  </SelectUf>

                  <Button type="submit"> Salvar </Button>
               </StyledForm>
            )}
         </Formik>
      </PopUp>
   )
}

const PopUp = styled.div`
   position: absolute;
   width: 100vw;
   height: 100vh;
   padding-top: 88px;
   background-color: rgba(0,0,0,.6);
   z-index: 1;
`

const StyledForm = styled(Form)`
   width: 90%;
   max-width: 400px;
   margin: 0 auto;
   padding: 20px;
   background-color: #FFF;
   border-radius: 8px;
`

const SelectUf = styled(Field)`
   border: 2px solid #DDD;
`

const Button = styled.button`
   margin-top: 15px;
   color: #FFF;
   background-color: #0a0;
   &:hover {
      color: #FFF;
      background-color: #080;
   }
`

export default ChangeLocation