import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { AuthContext } from '../../Contexts/AuthContext'
import { Restaurants } from '../../utils/Restaurants.json'
import RestaurantCard from './RestaurantCard'
import ChangeLocation from './ChangeLocation'

import SettingsIcon from '@material-ui/icons/Settings'
import EditLocationIcon from '@material-ui/icons/EditLocation'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'

const Main = () => {
   const { handleLogout } = useContext(AuthContext)

   const [uf, setUf] = useState(localStorage.getItem('UF') ?? 'DF')

   const [isPopUp, setIsPopUp] = useState(false)

   const handleEditLocation = () => setIsPopUp(true)

   return (
      <Container>
         <Content>
            <Header>
               <HeaderContainer>
                  <Logo> Reservas Online </Logo>
                  <div>
                     <EditLocation onClick={handleEditLocation} style={{ fontSize: 30 }} />
                     <Link to="/config">
                        <Settings style={{ fontSize: 30 }} />
                     </Link>
                     <Logout onClick={handleLogout} style={{ fontSize: 30 }} />
                  </div>
               </HeaderContainer>
            </Header>

            {isPopUp && <ChangeLocation isPopUp={isPopUp} setIsPopUp={setIsPopUp} setUf={setUf} />}

            <List>
               {Restaurants.map(({ id, UF, logo, name, stars, category, description, schedules }) =>
                  UF === uf &&
                  <RestaurantCard
                     key={id}
                     id={id}
                     logo={logo}
                     name={name}
                     stars={stars}
                     category={category}
                     description={description}
                     schedules={schedules}
                  />
               )}
            </List>
         </Content>
      </Container>
   )
}

const Container = styled.div`
   width: 100vw;
   height: 100vh;
   background-color: #FFF;
`

const Content = styled.div`
   min-width: 320px;
   height: 100vh;
   margin: 0 auto;
`

const Header = styled.header`
   position: fixed;
   width: 100vw;
   background-color: #F0F0FF;
   box-shadow: 0px 3px 10px 0px rgba(0,0,0,.2);
   z-index: 2;
`

const HeaderContainer = styled.div`
   /* --- FLEX --- */
   display: flex;
   justify-content: space-between;
   /*--------------*/
   max-width: 960px;
   margin: 0 auto;
   padding: 10px 12px 5px 15px;

   @media (min-width: 360px) { padding: 10px 16px 5px 20px }

   @media (min-width: 960px) { padding: 10px 8px 5px 10px }
`

const Logo = styled.h1`
   color: #004080;
   font-size: 24px;
   font-family: Girassol, sans-serif;
   line-height: 30px;
   cursor: default;
`

const EditLocation = styled(EditLocationIcon)`
   color: #667;
   cursor: pointer;
`

const Settings = styled(SettingsIcon)`
   margin-left: 15px;
   color: #667;
   cursor: pointer;
`

const Logout = styled(ExitToAppIcon)`
   margin-left: 15px;
   color: #C00;
   cursor: pointer;
`

const List = styled.ul`
   /* --- GRID --- */
   display: grid;
   grid-template-columns: 1fr;
   gap: 12px;
   /*--------------*/
   max-width: 960px;
   margin: 0 auto;
   padding-top: 68px;
   list-style: none;
   &:first-child { margin-top: 20px }

   @media (min-width: 750px) {
      grid-template-columns: repeat(2, 1fr)
   }
`

export default Main