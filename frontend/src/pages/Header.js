import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

const Header = () => {
   return (
      <Container>
         <Content>
            <Link to="/restaurantes"><StyledArrowBack style={{ fontSize: 28 }} /></Link>
            <Logo> Reservas Online </Logo>
         </Content>
      </Container>
   )
}

const Container = styled.header`
   position: fixed;
   width: 100%;
   padding: 11px 15px 6px 20px;
   background-color: #EEEEF8;
   box-shadow: 0px 2px 10px 0px rgba(50,50,50,.3);
   z-index: 1;
`

const Content = styled.div`
   /* --- FLEX --- */
   display: flex;
   justify-content: space-between;
   /*--------------*/
   max-width: 960px;
   margin: 0 auto;

   @media (min-width: 750px) { padding: 0 25px }
`

const StyledArrowBack = styled(ArrowBackIosIcon)`
   color: #667;
   cursor: pointer;
`

const Logo = styled.h1`
   color: #004080;
   font-size: 24px;
   font-family: Girassol, sans-serif;
   line-height: 28px;
   cursor: default;
`

export default Header