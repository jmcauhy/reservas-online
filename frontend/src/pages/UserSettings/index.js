import React from 'react'
import styled from 'styled-components'

import Header from '../Header'
import DataForm from './DataForm'
import PassForm from './PassForm'

const UserSettings = () => {
   return (
      <Container>
         <Content>
            <Header />

            <Main>
               <Title> Olá, João! </Title>

               <ReservationWarn>
                  Fique atento à sua reserva para hoje: <br/>
                  <strong> Restaurante: </strong> Coco Bambu <br/>
                  <strong> Horário: </strong> 21:00 <br/>
               </ReservationWarn>

               <DataForm />
               <PassForm />
            </Main>
         </Content>
      </Container>
   )
}

const Container = styled.div`
   width: 100%;
   height: 100vh;
   margin: 0 auto;
   background-color: #FFF;
`

const Content = styled.div`
   position: relative;
   min-width: 320px;
   height: 100vh;
   margin: 0 auto;
`

const Main = styled.main`
   position: relative;
   max-width: 960px;
   margin: 0 auto;
   padding-top: 48px;

   @media (min-width: 750px) {
      /* --- GRID --- */
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      gap: 20px;
      /*--------------*/
   }
`

const Title = styled.h1`
   margin: 20px 0 0 20px;
   color: #004080;
   font-size: 20px;
`

const ReservationWarn = styled.p`
   margin: 25px 20px 0 20px;
   padding: 10px 15px;
   color: #555;
   font-size: 14px;
   line-height: 26px;
   background-color: #fff3e6;
   border: 2px solid #F80;
   border-radius: 12px;
`

export default UserSettings