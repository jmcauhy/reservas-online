import React, { useState } from 'react'
import styled from 'styled-components'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'

const UpdatePasswordForm = () => {
   const [isVisible, setIsVisible] = useState(false),
         handlePasswordVisibility = () => setIsVisible(!isVisible)

   return (
      <Container>
         <Title> Alterar senha: </Title>
         <Formik
            initialValues={{ oldPassword: '',  newPassword: '',  newPasswordRepeat: '' }}

            validationSchema={
               Yup.object().shape({
                  oldPassword: Yup.string().min(8).max(32).required(),
                  newPassword: Yup.string().min(8).max(32).required(),
                  newPasswordRepeat: Yup.string().min(8).max(32).required()
               })
            }

            onSubmit={() => alert('Senha atualizada com sucesso!')}
         >
            {({ touched, errors }) => (
               <Form>
                  <PasswordContainer>
                     <PasswordInput
                        style={touched.oldPassword && errors.oldPassword && { borderColor: '#d00' }}
                        type={isVisible ? 'text' : 'password'}
                        name="oldPassword"
                        className="password"
                        maxLength="32"
                        placeholder="Senha atual"
                     />
                     <div onClick={handlePasswordVisibility}>
                        {isVisible ? <StyledVisibilityOffIcon /> : <StyledVisibilityIcon />}
                     </div>
                  </PasswordContainer>

                  <PasswordContainer>
                     <PasswordInput
                        style={touched.newPassword && errors.newPassword && { borderColor: '#d00' }}
                        type={isVisible ? 'text' : 'password'}
                        name="newPassword"
                        className="password"
                        maxLength="32"
                        placeholder="Nova senha"
                     />
                     <div onClick={handlePasswordVisibility}>
                        {isVisible ? <StyledVisibilityOffIcon /> : <StyledVisibilityIcon />}
                     </div>
                  </PasswordContainer>

                  <PasswordContainer>
                     <PasswordInput
                        style={touched.newPasswordRepeat && errors.newPasswordRepeat && { borderColor: '#d00' }}
                        type={isVisible ? 'text' : 'password'}
                        name="newPasswordRepeat"
                        className="password"
                        maxLength="32"
                        placeholder="Repita a nova senha"
                     />
                     <div onClick={handlePasswordVisibility}>
                        {isVisible ? <StyledVisibilityOffIcon /> : <StyledVisibilityIcon />}
                     </div>
                  </PasswordContainer>

                  <Button type="submit"> Alterar senha </Button>
               </Form>
            )}
         </Formik>
      </Container>
   )
}

const Container = styled.div`
   width: 100%;
   margin: 20px auto;
   padding: 20px;
   background-color: #E0E0FF;

   @media (min-width: 750px) {
      margin: 0;
      border-radius: 12px;
      /*--------------*/
      grid-column: span 1;
   }
`

const Title = styled.h1`
   color: #004080;
   font-size: 18px;
`

const PasswordContainer = styled.div`
   position: relative;
   height: 50px;
   margin-top: 15px;
`

const PasswordInput = styled(Field)`
   position: absolute;
   padding-right: 50px;
`

const StyledVisibilityIcon = styled(VisibilityIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const StyledVisibilityOffIcon = styled(VisibilityOffIcon)`
   position: absolute;
   top: 13px;
   right: 15px;
   color: #555;
   cursor: pointer;
`

const Button = styled.button`
   margin-top: 15px;
   color: #FFF;
   background-color: #F80;
   &:hover {
      color: #FFF;
      background-color: #cc6d00;
   }
`

export default UpdatePasswordForm