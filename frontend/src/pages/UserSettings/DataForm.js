import React from 'react'
import styled from 'styled-components'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

import { UFs } from '../../utils/UFs.json'

const UpdateDataForm = () => {
   return (
      <Container>
         <Title> Alterar dados: </Title>
         <Formik
            initialValues={{ name: '', uf: '', email: '' }}

            validationSchema={
               Yup.object().shape({
                  name: Yup.string().min(3).max(32),
                  uf: Yup.string(),
                  email: Yup.string().email().max(48)
               })
            }

            onSubmit={() => alert('Atualizado com sucesso!')}
         >
            {({ touched, errors }) => (
               <Form>
                  <NameContainer>
                     <Field
                        style={touched.name && errors.name && { borderColor: '#d00' }}
                        type="name"
                        name="name"
                        className="name"
                        maxLength="32"
                        placeholder="Nome"
                     />
                  </NameContainer>

                  <UfContainer>
                     <Field
                        component="select"
                        name="uf"
                        style={touched.uf && errors.uf && { borderColor: '#d00' }}
                     >
                        <option> Estado </option>
                        {UFs.map(uf => <option key={uf.id} value={uf.initials}> {uf.name} </option>)}
                     </Field>
                  </UfContainer>

                  <EmailContainer>
                     <Field
                        style={touched.email && errors.email && { borderColor: '#d00' }}
                        type="email"
                        name="email"
                        maxLength="48"
                        placeholder="E-mail"
                     />
                  </EmailContainer>

                  <Button type="submit"> Alterar informações </Button>
               </Form>
            )}
         </Formik>
      </Container>
   )
}

const Container = styled.div`
   width: 100%;
   margin: 20px auto;
   padding: 20px;
   background-color: #E0E0FF;

   @media (min-width: 750px) {
      margin: 0;
      border-radius: 12px;
      /*--------------*/
      grid-column: span 1;
   }
`

const Title = styled.h1`
   color: #004080;
   font-size: 18px;
`

const NameContainer = styled.div` margin-top:15px; `

const UfContainer = styled(NameContainer)``

const EmailContainer = styled(NameContainer)``

const Button = styled.button`
   margin-top: 15px;
   color: #FFF;
   background-color: #F80;
   &:hover {
      color: #FFF;
      background-color: #cc6d00;
   }
`

export default UpdateDataForm