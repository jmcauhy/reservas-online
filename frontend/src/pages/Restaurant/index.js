import React, { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import styled from 'styled-components'

import { Restaurants } from '../../utils/Restaurants.json'
import Header from '../Header'
import Reservation from './Reservation'

import WhatsAppIcon from '@material-ui/icons/WhatsApp'
import StarRateIcon from '@material-ui/icons/StarRate'
import PhoneForwardedIcon from '@material-ui/icons/PhoneForwarded'

const Restaurant = () => {
   const [restaurant, setRestaurant] = useState([]),
         { name, img, logo, stars, category, phone, description } = restaurant

   const queryParams = useLocation().search

   useEffect(() =>
      (() => {
         const data = Restaurants.filter(restaurant => queryParams.includes(restaurant.id))

         setRestaurant(data[0])
      })()
   , [queryParams])

   return (
      <>
         <Header />
         <Main>
            <Image img={img}></Image>

            <Info>
               <RestaurantLogo src={logo} alt={name} />

               <CategoryAndStars>
                  <Category> {category} </Category>
                  <Stars> <StarRateIcon /> {stars} </Stars>
               </CategoryAndStars>

               <Name> {name} </Name>

               <Description> {description} </Description>

               <Contact>
                  <WhatsApp href={`https://wa.me/55${phone}`} target="_blank" rel="noopener noreferrer">
                     <WhatsAppIcon style={{ marginRight: 5 }} /> WhatsApp
                  </WhatsApp>

                  <Phone><PhoneForwardedIcon style={{ marginRight: 5 }} />&nbsp;{phone} </Phone>
               </Contact>
            </Info>

            <Reservation restaurant={restaurant} />
         </Main>
      </>
   )
}

const Main = styled.main`
   /* --- GRID --- */
   display: grid;
   grid-template-columns: 1fr 1fr;
   /*--------------*/
   max-width: 960px;
   min-width: 320px;
   margin: 0 auto;
   background-color: #FFF;

   @media (min-width: 960px) {
      align-items: baseline;
      gap: 20px;
   }
`

const Image = styled.div`
   max-width: 960px;
   height: 200px;
   background-color: #EEEEF8;
   background-image: url(${props => props.img});
   background-size: cover;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) { height: 300px }

   @media (min-width: 960px) { border-radius: 0 0 12px 12px }
`

const Info = styled.div`
   /* --- GRID --- */
   display: grid;
   grid-template-columns: 1fr;
   /*--------------*/
   margin: 0 auto;
   padding-top: 50px;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) { grid-template-columns: repeat(12, 1fr) }

   @media (min-width: 960px) {
      grid-template-columns: repeat(12, 1fr);
      /*--------------*/
      margin:  0 10px 0 20px;
      padding: 0;
      /*--------------*/
      grid-column: span 1;
   }
`

const RestaurantLogo = styled.img`
   width: 90px;
   height: 90px;
   margin: -90px 0 0 20px;
   border-radius: 50%;
   box-shadow: 0px 3px 8px 0px rgba(0,0,0,.4);
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) {
      /*--------------*/
      grid-column: 1 / 2;
      grid-row: 1;
   }

   @media (min-width: 960px) {
      margin: -50px 0 0 0;
      width: 100px;
      height: 100px;
      /*--------------*/
      grid-column: 1 / 4;
   }
`

const CategoryAndStars = styled.div`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: flex-end;
   /*--------------*/
   margin: -50px 20px 0 20px;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) {
      /*--------------*/
      margin: -36px 20px 0 0;
      /*--------------*/
      grid-column: 10 / -1;
      grid-row: 1;
   }

   @media (min-width: 960px) {
      justify-content: space-between;
      /*--------------*/
      margin: 20px 0 0 15px;
      /*--------------*/
      grid-column: 4 / -1;
   }
`

const Category = styled.p`
   margin: 0 8px 0 0;
   color: #666;
   font-size: 13px;
`

const Stars = styled.p`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   /*--------------*/
   color: #FA0;
   font-size: 15px;
`

const Name = styled.h1`
   margin: 20px 0 0 20px;
   color: #004080;
   font-size: 18px;
   font-family: Roboto, sans-serif;
   letter-spacing: .5px;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 360px) { font-size: 20px }

   @media (min-width: 750px) {
      margin: -30px 0 0 20px;
      /*--------------*/
      grid-column: 2 / 10;
      grid-row: 1;
   }

   @media (min-width: 960px) {
      margin: -5px 0 0 15px;
      /*--------------*/
      grid-column: 4 / -1;
   }
`

const Contact = styled.div`
   /* --- GRID --- */
   display: grid;
   grid-template-columns: 1fr 1fr;
   gap: 15px;
   /*--------------*/
   margin: 0 20px;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) {
      margin: 20px 20px 0 0;
      /*--------------*/
      grid-column: 9 / -1;
      grid-row: 3;
   }

   @media (min-width: 960px) {
      margin: 20px 0 0 0;
      /*--------------*/
      grid-column: 1 / -1;
   }
`

const WhatsApp = styled.a`
   /* --- FLEX --- */
   display: flex;
   align-items: center;
   justify-content: center;
   /*--------------*/
   width: 100%;
   height: 40px;
   color: #25D366;
   font-size: 15px;
   background-color: transparent;
   border: 2px solid #25D366;
   border-radius: 12px;
   /*--------------*/
   grid-column: span 1;

   @media (min-width: 750px) { grid-column: 1 / -1 }

   @media (min-width: 960px) {
      margin: 0 20px 0 0;
      /*--------------*/
      grid-column: span 1;
   }
`

const Phone = styled.button`
   height: 40px;
   color: #004080;
   font-size: 15px;
   background-color: transparent;
   border: 2px solid #004080;
   border-radius: 12px;
   /*--------------*/
   grid-column: span 1;

   @media (min-width: 750px) { grid-column: 1 / -1 }

   @media (min-width: 960px) {
      margin: 0;
      /*--------------*/
      grid-column: span 1;
   }
`

const Description = styled.p`
   margin: 15px 20px 20px 20px;
   text-align: justify;
   color: #889;
   font-size: 13px;
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 360px) { font-size: 14px }

   @media (min-width: 750px) {
      margin: 20px 20px 0 20px;
      /*--------------*/
      grid-column: 1 / 9;
      grid-row: 2;
   }

   @media (min-width: 960px) {
      margin: 15px 0 0 0;
      /*--------------*/
      grid-column: 1 / -1;
   }
`

export default Restaurant