import React, { useState } from 'react'
import styled from 'styled-components'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import AlarmIcon from '@material-ui/icons/Alarm'
import CancelIcon from '@material-ui/icons/Cancel'

const Reservation = ({ restaurant: { name, schedules } }) => {
   const [isReserved, setIsReserved] = useState(false),
         [date, setDate] = useState(''),
         [hour, setHour] = useState('')

   const handleReserve = () => setIsReserved(true)

   const handleCancel = () => setIsReserved(false)

   return (
      !isReserved ?
      <Container>
         <Title> Agende sua reserva </Title>

         <StyledDatePicker
            dateFormat="dd/MM/yyyy"
            minDate={new Date().setDate(new Date().getDate() +  1)}
            maxDate={new Date().setDate(new Date().getDate() + 29)}
            selected={date}
            onChange={setDate}
            placeholderText="Escolha uma data"
         />

         {  date &&
         <> <Subtitle> Almoço </Subtitle>
            <Schedules>
               {schedules.map(({ id, hour }) => id <= 8 &&
                  <Option key={id} tabIndex="0" onClick={e => setHour(e.target.textContent)}>{hour}</Option>
               )}
            </Schedules>

            <Subtitle> Jantar </Subtitle>
            <Schedules>
               {schedules.map(({ id, hour }) => id >= 9 &&
                  <Option key={id} tabIndex="0" onClick={e => setHour(e.target.textContent)}>{hour}</Option>
               )}
            </Schedules> </>
         }

         <ReserveButton onClick={handleReserve}>
            Reservar&nbsp;<AlarmIcon />
         </ReserveButton>
      </Container>
      :
      <Container>
         <Title>Reservado com sucesso!</Title>
         <br/>
         <Message>
            Pronto, você já está reservado(a) para hoje, às <strong>{hour}</strong> no <strong>{name}</strong>.
            <br/><br/>
            <strong>Atenção:</strong> Chegue no horário marcado para evitar o cancelamento da sua reserva.
            <br/><br/>
            Bom apetite!
         </Message>

         <CancelButton onClick={handleCancel}>
            Cancelar&nbsp;<CancelIcon />
         </CancelButton>
      </Container>
   )
}

const Container = styled.div`
   margin: 20px 20px 50px 20px;
   padding: 20px;
   background-color: #FFF;
   border-radius: 12px;
   box-shadow: 0px 0px 10px 0px rgba(50,50,50,.35);
   /*--------------*/
   grid-column: 1 / -1;

   @media (min-width: 750px) {
      margin: 20px;
      /*--------------*/
      grid-column: 1 / -1;
   }

   @media (min-width: 960px) {
      margin: 0 20px 0 10px;
      /*--------------*/
      grid-column: span 1;
   }
`

const Title = styled.h2`
   text-align: center;
   color: #004080;
   font-weight: bold;
   font-size: 18px;

   @media (min-width: 360px) { font-size: 20px }
`

const StyledDatePicker = styled(DatePicker)`
   margin-top: 15px;
   text-align: center;
   color: #667;
   border-color: #ccd;
   cursor: default;
`

const Subtitle = styled.p`
   margin: 15px 0 5px 6px;
   text-align: center;
   color: #004080;
   font-weight: bold;
   font-size: 13px;

   @media (min-width: 360px) { font-size: 14px }
`

const Schedules = styled.div`
   /* --- FLEX --- */
   display: flex;
   justify-content: center;
   flex-wrap: wrap;
   /*--------------*/

   @media (min-width: 960px) { justify-content: center; }
`

const Option = styled.div`
   margin: 5px;
   padding: 3px 6px;
   color: #333;
   font-size: 13px;
   background-color: #EEEEF8;
   border-radius: 5px;
   cursor: pointer;
   transition: .2s;
   border: 2px solid #EEEEF8;
   &:hover { background-color: #E5E5FF }
   &:focus { border: 2px solid #004080; background-color: #E5E5FF }
`

const Button = styled.button`
   width: 100%;
   margin: 20px auto -45px auto;
   border-radius: 100px;
   box-shadow: 0px 3px 5px 0px rgba(50,50,50,.35);
   transition: .2s;
`

const ReserveButton = styled(Button)`
   background-color: #0A0;
   &:hover { background-color: #080; }
`

const Message = styled.p`
   font-size: 14px;
`

const CancelButton = styled(Button)`
   background-color: #C00;
   &:hover { background-color: #A00; }
`

export default Reservation