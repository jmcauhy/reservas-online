import { Router } from 'express'

import UserShowValidator from './validators/User/UserShowValidator'
import UserCreateValidator from './validators/User/UserCreateValidator'
import UserUpdateValidator from './validators/User/UserUpdateValidator'
import RestaurantShowValidator from './validators/Restaurant/RestaurantShowValidator'
import RestaurantListValidator from './validators/Restaurant/RestaurantListValidator'
import RestaurantCreateValidator from './validators/Restaurant/RestaurantCreateValidator'
import ReservationShowValidator from './validators/Reservation/ReservationShowValidator'
import ReservationListValidator from './validators/Reservation/ReservationListValidator'
import ReservationCreateValidator from './validators/Reservation/ReservationCreateValidator'
import ReservationDeleteValidator from './validators/Reservation/ReservationDeleteValidator'

import AuthController from './controllers/AuthController'
import UserController from './controllers/UserController'
import ReservationController from './controllers/ReservationController'
import RestaurantController from './controllers/RestaurantController'

const routes = Router()

const authController = new AuthController,
      userController = new UserController,
      reservationController = new ReservationController,
      restaurantController = new RestaurantController

// Authentication route
routes.post('/auth', authController.auth)

// User routes
routes.get('/user/:id', UserShowValidator, userController.show)
routes.get('/users', userController.list)
routes.post('/user', UserCreateValidator, userController.create)
routes.put('/user', UserUpdateValidator, userController.update)

// Reservation routes
routes.get('/reservation/:id', ReservationShowValidator, reservationController.show)
routes.get('/reservations', ReservationListValidator, reservationController.list)
routes.post('/reservation', ReservationCreateValidator, reservationController.create)
routes.delete('/reservation/:id', ReservationDeleteValidator, reservationController.delete)

// Restaurant routes
routes.get('/restaurant/:id', RestaurantShowValidator, restaurantController.show)
routes.get('/restaurants', RestaurantListValidator, restaurantController.list)
routes.post('/restaurant', RestaurantCreateValidator, restaurantController.create)

export default routes