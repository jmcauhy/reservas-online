import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const ReservationCreateValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      timestamp: Yup.date().required(),
      userId: Yup.string().required(),
      restaurantId: Yup.string().required()
   })

   const validation = await schema.validate(req.body)
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default ReservationCreateValidator