import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const ReservationShowValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      id: Yup.string().required()
   })

   const validation = await schema.validate(req.params)
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default ReservationShowValidator