import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const ReservationListValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      id: Yup.string().required()
   })

   const validation = await schema.validate(req.query)
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default ReservationListValidator