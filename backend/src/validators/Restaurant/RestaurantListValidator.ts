import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const RestaurantListValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      uf: Yup.string().length(2).required()
   })

   const validation = await schema.validate(req.query)
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default RestaurantListValidator