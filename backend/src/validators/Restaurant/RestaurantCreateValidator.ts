import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const RestaurantCreateValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      name: Yup.string().min(2).max(50).required(),
      uf: Yup.string().length(2).required(),
      img: Yup.string().url().required(),
      logo: Yup.string().url().required(),
      stars: Yup.number().min(1).max(5).required(),
      coords: Yup.array().required(),
      whatsapp: Yup.string().required(),
      category: Yup.string().required(),
      description: Yup.string().required(),
      reservations: Yup.array().required(),
      openTime: Yup.array().required()
   })

   const validation = await schema.validate(req.body, { abortEarly: false })
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default RestaurantCreateValidator