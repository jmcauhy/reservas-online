import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const UserUpdateValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      name: Yup.string().min(2).max(100),
      uf: Yup.string().length(2),
      email: Yup.string().max(100).email(),
      newPassword: Yup.string().min(8)
   })

   const validation = await schema.validate(req.body, { abortEarly: false })
      .catch(err => err)

   if (!validation.errors) return next()

   return res.json(validation.inner)
}

export default UserUpdateValidator