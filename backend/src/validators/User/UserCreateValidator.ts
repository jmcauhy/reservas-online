import { Request, Response, NextFunction } from 'express'
import * as Yup from 'yup'

const UserCreateValidator = async (req: Request, res: Response, next: NextFunction) => {
   const schema = Yup.object().shape({
      name: Yup.string().min(2).max(100).required(),
      uf: Yup.string().length(2).required(),
      email: Yup.string().email().required(),
      password: Yup.string().required()
   })

   const validation = await schema.validate(req.body, { abortEarly: false })
      .catch(err => err)

   if (!validation.errors) return next()

   return res.status(400).json(validation.inner)
}

export default UserCreateValidator