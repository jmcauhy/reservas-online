import { MigrationInterface, QueryRunner } from "typeorm";

export class createUserAndRestaurant1595370514580 implements MigrationInterface {
   name = 'createUserAndRestaurant1595370514580'

   public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`CREATE TABLE "restaurant" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "uf" character varying NOT NULL, "img" character varying NOT NULL, "logo" character varying NOT NULL, "stars" integer NOT NULL, "coords" point array NOT NULL, "whatsapp" character varying NOT NULL, "category" character varying NOT NULL, "description" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_649e250d8b8165cb406d99aa30f" PRIMARY KEY ("id"))`);
      await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "uf" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "reservation" json NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
   }

   public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`DROP TABLE "user"`);
      await queryRunner.query(`DROP TABLE "restaurant"`);
   }

}