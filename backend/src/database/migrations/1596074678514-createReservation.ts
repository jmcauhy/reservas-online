import { MigrationInterface, QueryRunner } from "typeorm";

export class createReservation1596074678514 implements MigrationInterface {
   name = 'createReservation1596074678514'

   public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`CREATE TABLE "reservation" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "timestamp" TIMESTAMP NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_48b1f9922368359ab88e8bfa525" PRIMARY KEY ("id"))`);
      await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "reservation"`);
      await queryRunner.query(`ALTER TABLE "restaurant" ADD "openTime" tsrange array NOT NULL`);
   }

   public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`ALTER TABLE "restaurant" DROP COLUMN "openTime"`);
      await queryRunner.query(`ALTER TABLE "user" ADD "reservation" json NOT NULL`);
      await queryRunner.query(`DROP TABLE "reservation"`);
   }

}