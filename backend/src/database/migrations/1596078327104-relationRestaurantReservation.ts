import {MigrationInterface, QueryRunner} from "typeorm";

export class relationRestaurantReservation1596078327104 implements MigrationInterface {
    name = 'relationRestaurantReservation1596078327104'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reservation" ADD "restaurantId" uuid`);
        await queryRunner.query(`ALTER TABLE "reservation" ADD CONSTRAINT "FK_2a2d6c09d1469e65c347513256a" FOREIGN KEY ("restaurantId") REFERENCES "restaurant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reservation" DROP CONSTRAINT "FK_2a2d6c09d1469e65c347513256a"`);
        await queryRunner.query(`ALTER TABLE "reservation" DROP COLUMN "restaurantId"`);
    }

}
