import {MigrationInterface, QueryRunner} from "typeorm";

export class relationUserReservation1596074961668 implements MigrationInterface {
    name = 'relationUserReservation1596074961668'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reservation" ADD "userId" uuid`);
        await queryRunner.query(`ALTER TABLE "reservation" ADD CONSTRAINT "UQ_529dceb01ef681127fef04d755d" UNIQUE ("userId")`);
        await queryRunner.query(`ALTER TABLE "reservation" ADD CONSTRAINT "FK_529dceb01ef681127fef04d755d" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reservation" DROP CONSTRAINT "FK_529dceb01ef681127fef04d755d"`);
        await queryRunner.query(`ALTER TABLE "reservation" DROP CONSTRAINT "UQ_529dceb01ef681127fef04d755d"`);
        await queryRunner.query(`ALTER TABLE "reservation" DROP COLUMN "userId"`);
    }

}
