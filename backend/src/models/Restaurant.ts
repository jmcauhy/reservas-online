import {
   Entity,
   PrimaryGeneratedColumn,
   Column,
   CreateDateColumn,
   UpdateDateColumn,
   OneToMany
} from 'typeorm'
import Reservation from './Reservation'

@Entity()
export default class Restaurant {

   @PrimaryGeneratedColumn("uuid")
   id: string

   @Column()
   name: string

   @Column()
   uf: string

   @Column()
   img: string

   @Column()
   logo: string

   @Column()
   category: string

   @Column()
   stars: number

   @Column("point", { array: true })
   coords: number[]

   @Column()
   whatsapp: string

   @Column()
   description: string

   @Column("tsrange", { array: true })
   openTime: Date[]

   @CreateDateColumn()
   created_at: Date

   @UpdateDateColumn()
   updated_at: Date

   @OneToMany(type => Reservation, restaurant => Restaurant)
   reservations: Reservation[]

}