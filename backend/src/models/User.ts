import {
   Entity,
   PrimaryGeneratedColumn,
   Column,
   BeforeInsert,
   BeforeUpdate,
   CreateDateColumn,
   UpdateDateColumn,
   OneToOne
} from 'typeorm'
import bcrypt from 'bcryptjs'
import Reservation from './Reservation'

@Entity()
export default class User {

   @PrimaryGeneratedColumn("uuid")
   id: string

   @Column()
   name: string

   @Column()
   uf: string

   @Column()
   email: string

   @Column()
   password: string

   @BeforeInsert()
   @BeforeUpdate()
   hashPassword() {
      this.password = bcrypt.hashSync(this.password, 8)
   }

   @CreateDateColumn()
   created_at: Date

   @UpdateDateColumn()
   updated_at: Date

   @OneToOne(type => Reservation, user => User)
   reservation: Reservation

}