import {
   Entity,
   PrimaryGeneratedColumn,
   CreateDateColumn,
   Column,
   OneToOne,
   JoinColumn,
   ManyToOne
} from 'typeorm'
import User from './User'
import Restaurant from './Restaurant'

@Entity()
export default class Reservation {

   @PrimaryGeneratedColumn("uuid")
   id: string

   @Column()
   timestamp: Date

   @CreateDateColumn()
   created_at: Date

   @OneToOne(type => User, reservation => Reservation)
   @JoinColumn()
   user: User

   @ManyToOne(type => Restaurant, reservations => Reservation)
   @JoinColumn()
   restaurant: Restaurant

}