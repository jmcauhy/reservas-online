import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'
import routes from './routes'
import 'reflect-metadata'
import './database'

const app = express()
dotenv.config()

app.use(express.json())
app.use(cors())
app.use(routes)

const PORT = process.env.PORT

app.listen(PORT, () => console.log(`-> Up and running on port ${PORT}`))

export default app