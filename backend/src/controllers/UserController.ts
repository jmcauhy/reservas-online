import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import User from '../models/User'

class UserController  {
   async show(req: Request, res: Response) {
      try {
         const repo = getRepository(User)

         const data = await repo.find(req.params)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }

   async list(req: Request, res: Response) {
      try {
         const repo = getRepository(User)

         const data = await repo.find()

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }

   async create(req: Request, res: Response) {
      try {
         const repo = getRepository(User)

         const { name, uf, email, password } = req.body

         const isRegistered = await repo.findOne({ where: { email } })

         if (isRegistered) return res.sendStatus(409)

         const user = repo.create({ name, uf, email, password })
         await repo.save(user)

         return res.json(user)
      }
      catch(err) { console.log(err.message) }
   }

   async update(req: Request, res: Response) {
      try {
         const repo = getRepository(User)

         const data = await repo.save(req.body)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }
}

export default UserController