import { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { getRepository } from 'typeorm'
import User from '../models/User'

class AuthController  {
   async auth(req: Request, res: Response) {
      try {
         const repo = getRepository(User)

         const { email, password } = req.body

         const user = await repo.findOne({ where: { email } })

         if (!user) return res.sendStatus(401)

         const validatePassword = await bcrypt.compare(password, user.password)

         if (!validatePassword) return res.sendStatus(401)

         // @ts-expect-error
         const token = jwt.sign({ id: user.id }, process.env.SECRET, { expiresIn: '1d' })

         // @ts-expect-error
         delete user.password

         return res.json({ user, token })
      }
      catch {
         return res.sendStatus(401)
      }
   }
}

export default AuthController