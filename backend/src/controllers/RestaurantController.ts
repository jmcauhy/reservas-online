import { Request, Response } from 'express'

class RestaurantController  {
   async show(req: Request, res: Response) {
      try {
         const { id } = req.params

         return res.json({ msg: 'show' })
      }
      catch(err) { console.log(err.message) }
   }

   async list(req: Request, res: Response) {
      try {
         const { uf } = req.query

         return res.json({ msg: 'list' })
      }
      catch(err) { console.log(err.message) }
   }

   async create(req: Request, res: Response) {
      try {
         return res.json({ msg: 'create' })
      }
      catch(err) { console.log(err.message) }
   }

   async update(req: Request, res: Response) {
      try {
         return res.json({ msg: 'update' })
      }
      catch(err) { console.log(err.message) }
   }
}

export default RestaurantController