import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import Reservation from '../models/Reservation'

class ReservationController  {
   async show(req: Request, res: Response) {
      try {
         const repo = getRepository(Reservation)

         const data = await repo.find(req.params)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }

   async list(req: Request, res: Response) {
      try {
         const repo = getRepository(Reservation)

         const data = await repo.find(req.query)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }

   async create(req: Request, res: Response) {
      try {
         const repo = getRepository(Reservation)

         const data = await repo.save(req.body)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }

   async delete(req: Request, res: Response) {
      try {
         const repo = getRepository(Reservation)

         const data = await repo.delete(req.params)

         return res.json(data)
      }
      catch(err) { console.log(err.message) }
   }
}

export default ReservationController